FROM composer:latest
ARG VER=2.0.1
ARG user=ibuser

RUN addgroup -S ${user} && adduser -S -G ${user} ${user} &&\
    apk add --no-cache git curl wget jq

RUN composer global require pheromone/phpcs-security-audit:${VER}
RUN /tmp/vendor/bin/phpcs --config-set installed_paths /tmp/vendor/pheromone/phpcs-security-audit/
ADD base_ruleset.xml /tmp/base_ruleset.xml 

RUN mkdir /code && chown -R ${user}:${user} /code

WORKDIR /tmp
USER ${user}
